const mongoose = require('mongoose');
const Order = require('../models/order');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const should = chai.should();

chai.use(chaiHttp);

describe('Orders', () => {
  beforeEach((done) => {
    Order.remove({}, (err) => {
      done();
    });
  });

  describe('/GET order', () => {
    it('it should GET all the orders', (done) => {
      chai.request(server)
        .get('/api/orders')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.eql(0);
          done();
        });
    });
  });

  describe('/POST order', () => {
    it('it should POST order', (done) => {
      let order = {
        name: 'John Doe',
        bags: 4,
      };
      chai.request(server)
        .post('/api/orders')
        .send(order)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('_id');
          res.body.should.have.property('name');
          res.body.should.have.property('bags');
          res.body.should.have.property('createdAt');
          done();
        });
    });

    it('it should not POST invalid order', (done) => {
      let order = {
        name: 'Johny Doe',
        bags: 6,
      };
      chai.request(server)
        .post('/api/orders')
        .send(order)
        .end((err, res) => {
          res.should.have.status(500);
          done();
        });
    });
  });

});