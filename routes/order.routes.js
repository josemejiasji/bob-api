'use strict';

const Router = require('express').Router;
const Order = require('../models/order');

const orderRouter = new Router();

orderRouter.get('/api/orders', (req, res, next) => {
  Order.find({})
    .then(orders => res.send(orders))
    .catch(next);
});

orderRouter.post('/api/orders', (req, res, next) => {
  Order.create(req.body)
    .then(order => res.send(order))
    .catch(next);
});

module.exports = orderRouter;
