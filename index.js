'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

const app = express();
const router = express.Router();

const PORT = process.env.PORT || 3001;
const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost/bob';

mongoose.Promise = Promise;
mongoose.connect(MONGODB_URI, {useNewUrlParser: true});

app.use(bodyParser.json(), cors());
app.use(require('./routes/order.routes'));

app.start = () => {
  app.listen(PORT, () => {
    console.log(`Server up and running at ${PORT}`);
  })
};

if (require.main === module) {
  app.start();
}

module.exports = app;
