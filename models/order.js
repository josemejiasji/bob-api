'use strict';

const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
  name: {type: String, required: true},
  bags: {type: Number, required: true, min: 1, max: 5},
  createdAt: {type: Date, default: Date.now},
});

module.exports = mongoose.model('Order', orderSchema);
